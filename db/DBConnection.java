package db;

import java.sql.*;
import java.util.Properties;

/**
 * Created by dimas on 28.06.17.
 */
public class DBConnection {




    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch (ClassNotFoundException e){
            //logger.log(Level.INFO, e.getMessage());
        }
    }

    public static Connection getConnection(){

        String URL = "jdbc:mysql://localhost:3306/work_relationship";
        Properties info = new Properties();
        info.put( "user", "root" );
        info.put( "password", "111111" );
        Connection conn = null;
        try{
            conn = DriverManager.getConnection(URL, info);
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }
        return conn;
    }

    public static void closeConnection(Connection connection){
        if(connection != null){
            try{
                connection.close();
            }catch (SQLException e){
                //logger.log(Level.INFO, e.getMessage());
            }
        }
    }


    public static void closeStatement(Statement statement){
        if(statement != null){
            try{
                statement.close();
            }catch (SQLException e){
                //logger.log(Level.INFO, e.getMessage());
            }
        }
    }

    public static void closeResultSet(ResultSet resultSet){
        if(resultSet != null){
            try{
                resultSet.close();
            }catch (SQLException e){
                //logger.log(Level.INFO, e.getMessage());
            }
        }
    }


}
