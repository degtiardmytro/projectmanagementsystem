package db.entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 */
public class Customers {
    private int id=0;
    private String name;
    private String surname;
    private String email;
    private String address;
    private String phone;

    private List<Projects> projects;


    /**
     *
     * @param name -
     * @param surname -
     * @param email -
     * @param address -
     * @param phone -
     */
    public Customers(String name, String surname, String email, String address, String phone) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.projects = new ArrayList<>();
    }

    /**
     *
     * @param id -
     * @param name -
     * @param surname -
     * @param email -
     * @param address -
     * @param phone -
     */
    public Customers(int id, String name, String surname, String email, String address, String phone) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.projects = new ArrayList<>();
    }

    public Customers(int id, String name, String surname, String email, String address, String phone, List<Projects> projects) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.projects = projects;
    }

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Projects> getProjects() {
        return projects;
    }

    public void setProjects(List<Projects> projects) {
        this.projects = projects;
    }

    public void addProject(Projects project){ this.projects.add(project);}

    @Override
    public String toString() {

        StringBuilder projStr = new StringBuilder();
        if(projects != null) {
            Iterator<Projects> projectIterator = projects.iterator();
            while (projectIterator.hasNext()) {
                projStr.append(projectIterator.next().getName());
                if (projectIterator.hasNext()) {
                    projStr.append(", ");
                }
            }
        }

        return String.format("Customers{id=%d, name='%s', surname='%s', email='%s', address='%s', phone=[%s], projects=[%s]}",
                id,
                name,
                surname,
                email,
                address,
                phone,
                projStr
        );
    }
}
