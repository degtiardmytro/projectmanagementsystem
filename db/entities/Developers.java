package db.entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 */
public class Developers {
    private int id;
    private String name;
    private String skype;
    private String phone;
    private float salary;

    private List<Skills> skills;
    private List<Projects> projects;

    /**
     * конструктор
     * @param name -
     * @param skype -
     * @param phone -
     * @param salary -
     */
    public Developers(String name, String skype, String phone, float salary) {
        this.name = name;
        this.skype = skype;
        this.phone = phone;
        this.salary = salary;
        this.skills = new ArrayList<>();
        this.projects = new ArrayList<>();
    }

    public Developers(String name, String skype, String phone, float salary, List<Skills> skills, List<Projects> projects) {
        this.name = name;
        this.skype = skype;
        this.phone = phone;
        this.salary = salary;
        this.skills = skills;
        this.projects = projects;
    }

    public Developers(int id, String name, String skype, String phone, float salary, List<Skills> skills, List<Projects> projects) {
        this.id = id;
        this.name = name;
        this.skype = skype;
        this.phone = phone;
        this.salary = salary;
        this.skills = skills;
        this.projects = projects;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public List<Skills> getSkills() {
        return skills;
    }

    public void setSkills(List<Skills> skills) {
        this.skills = skills;
    }

    public void addSkill(Skills skill){
        skills.add(skill);
    }


    public List<Projects> getProjects() {
        return projects;
    }

    public void setProjects(List<Projects> projects) {
        this.projects = projects;
    }

    public void addProject(Projects project){
        projects.add(project);
    }


    @Override
    public String toString() {
        StringBuilder skillStr = new StringBuilder();
        if(skills != null) {
            Iterator<Skills> skillsIterator = skills.iterator();
            while (skillsIterator.hasNext()) {
                skillStr.append(skillsIterator.next().getName());
                if (skillsIterator.hasNext()) {
                    skillStr.append(", ");
                }
            }
        }

        StringBuilder projStr = new StringBuilder();
        if(projects != null) {
            Iterator<Projects> projectIterator = projects.iterator();
            while (projectIterator.hasNext()) {
                projStr.append(projectIterator.next().getName());
                if (projectIterator.hasNext()) {
                    projStr.append(", ");
                }
            }
        }

        return String.format("Developers{id=%d, name='%s', skype='%s', phone='%s', salary=%f, skills=[%s], projects=[%s]}",
                id,
                name,
                skype,
                phone,
                salary,
                skillStr,
                projStr
        );
    }
}
