package db.entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 */
public class Projects {

    private int id = 0;
    private String name;
    private int deadline;
    private float cost;

    private List<Developers> developers;
    private List<Companies> companies;
    private List<Customers> customers;

    /**
     *
     * @param name -
     * @param deadline -
     * @param cost -
     */
    public Projects(String name, int deadline, float cost) {
        this.name = name;
        this.deadline = deadline;
        this.cost = cost;
        this.developers = new ArrayList<>();
        this.companies = new ArrayList<>();
        this.customers = new ArrayList<>();
    }

    public Projects(int id, String name, int deadline, float cost) {
        this.id = id;
        this.name = name;
        this.deadline = deadline;
        this.cost = cost;
        this.developers = new ArrayList<>();
        this.companies = new ArrayList<>();
        this.customers = new ArrayList<>();
    }

    public Projects(int id, String name, int deadline, float cost, List<Developers> developers, List<Companies> companies, List<Customers> customers) {
        this.id = id;
        this.name = name;
        this.deadline = deadline;
        this.cost = cost;
        this.developers = developers;
        this.companies = companies;
        this.customers = customers;
    }

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDeadline() {
        return deadline;
    }

    public void setDeadline(int deadline) {
        this.deadline = deadline;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }


    public List<Developers> getDevelopers() { return developers;}

    public void setDevelopers(List<Developers> developers) { this.developers = developers; }

    public void addDeveloper(Developers developer){this.developers.add(developer);}

    public List<Companies> getCompanies() { return companies; }

    public void setCompanies(List<Companies> companies) { this.companies = companies; }

    public void addCompany(Companies company){this.companies.add(company);}

    public List<Customers> getCustomers() { return customers; }

    public void setCustomers(List<Customers> customers) { this.customers = customers; }

    public void addCustomer(Customers customer){this.customers.add(customer);}


    @Override
    public String toString() {

        //список разработчиков
        StringBuilder developerStr = new StringBuilder();
        if(developers != null) {
            Iterator<Developers> developersIterator = developers.iterator();
            while (developersIterator.hasNext()) {
                developerStr.append(developersIterator.next().getName());
                if (developersIterator.hasNext()) {
                    developerStr.append(", ");
                }
            }
        }

        //список компаний
        StringBuilder companyStr = new StringBuilder();
        if(companies != null) {
            Iterator<Companies> companiesIterator = companies.iterator();
            while (companiesIterator.hasNext()) {
                companyStr.append(companiesIterator.next().getName());
                if (companiesIterator.hasNext()) {
                    companyStr.append(", ");
                }
            }
        }

        //список заказчиков
        StringBuilder customerStr = new StringBuilder();
        if(customers != null) {
            Iterator<Customers> customersIterator = customers.iterator();
            while (customersIterator.hasNext()) {
                customerStr.append(customersIterator.next().getName());
                if (customersIterator.hasNext()) {
                    customerStr.append(", ");
                }
            }
        }

        return String.format("Projects{id=%d, name='%s', deadline=%d, cost=%f, developers=[%s], companies=[%s], customers=[%s]}",
                id,
                name,
                deadline,
                cost,
                developerStr,
                companyStr,
                customerStr
                );
    }
}
