package db.entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 */
public class Skills {

    private int id;
    private String name;

    private List<Developers> developers;


    /**
     *
     * @param name -
     */
    public Skills(String name) {

        this.name = name;
        this.developers = new ArrayList<>();
    }

    /**
     *
     * @param id -
     * @param name -
     */
    public Skills(int id, String name) {
        this.id = id;
        this.name = name;
        this.developers = new ArrayList<>();
    }

    /**
     *
     * @param id -
     * @param name -
     * @param developers -
     */
    public Skills(int id, String name, List<Developers> developers) {
        this.id = id;
        this.name = name;
        this.developers = developers;
    }

    /**
     *
     * @param name -
     * @param developers -
     */
    public Skills(String name, List<Developers> developers) {

        this.name = name;
        this.developers = developers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Developers> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developers> developers) {
        this.developers = developers;
    }

    @Override
    public String toString() {

        StringBuilder devStr = new StringBuilder();
        if(developers != null) {
            Iterator<Developers> developersIterator = developers.iterator();
            while (developersIterator.hasNext()) {
                devStr.append(developersIterator.next().getName());
                if (developersIterator.hasNext()) {
                    devStr.append(", ");
                }
            }
        }
        return String.format("Skills{id=%d, name='%s', developers=[%s]}", id, name, devStr);
    }
}
