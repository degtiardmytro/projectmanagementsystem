package db.entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 */
public class Companies {

    private int id = 0;
    private String name;
    private String registrationNumber;
    private String phone;
    private String address;

    private List<Projects> projects;


    public Companies(String name, String registrationNumber, String phone, String address) {
        this.name = name;
        this.registrationNumber = registrationNumber;
        this.phone = phone;
        this.address = address;
        this.projects = new ArrayList<>();
    }


    public Companies(int id, String name, String registrationNumber, String phone, String address) {
        this.id = id;
        this.name = name;
        this.registrationNumber = registrationNumber;
        this.phone = phone;
        this.address = address;
        this.projects = new ArrayList<>();
    }


    public Companies(int id, String name, String registrationNumber, String phone, String address, List<Projects> projects) {
        this.id = id;
        this.name = name;
        this.registrationNumber = registrationNumber;
        this.phone = phone;
        this.address = address;
        this.projects = projects;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Projects> getProjects() {
        return projects;
    }

    public void setProjects(List<Projects> projects) {
        this.projects = projects;
    }

    public void addProject(Projects project){
        this.projects.add(project);
    }


    @Override
    public String toString() {

        StringBuilder projStr = new StringBuilder();
        if(projects != null) {
            Iterator<Projects> projectIterator = projects.iterator();
            while (projectIterator.hasNext()) {
                projStr.append(projectIterator.next().getName());
                if (projectIterator.hasNext()) {
                    projStr.append(", ");
                }
            }
        }

        return String.format("Companies{id=%d, name='%s', registrationNumber='%s', phone='%s', address='%s', projects=[%s]}",
                id,
                name,
                registrationNumber,
                phone,
                address,
                projStr
        );
    }
}
