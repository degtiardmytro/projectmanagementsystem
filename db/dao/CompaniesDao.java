package db.dao;

import db.DBConnection;
import db.entities.Companies;
import db.entities.Projects;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 */
public class CompaniesDao implements BaseDao<Integer, Companies> {

    private Connection connection;

    private static final String CREATE_SQL          = "INSERT INTO `companies` (`name`, `registration_number`,`phone`,`address`) VALUES (?,?,?,?)";
    private static final String READ_SQL            = "SELECT * FROM `companies` WHERE `id_companies`=?";
    private static final String UPDATE_SQL          = "UPDATE `companies` SET `name`=?, `registration_number`=?, `phone`=?,`address`=? WHERE  `id_companies`  = ?";
    private static final String DELETE_SQL          = "DELETE FROM `companies` WHERE  `id_companies`  = ?";
    private static final String GET_ALL_SQL         = "SELECT * FROM companies";
    private static final String GET_ALL_BY_NAME_SQL = "SELECT * FROM `companies` WHERE `name` LIKE ?";
    private static final String GET_ONE_BY_NAME_SQL = "SELECT * FROM `companies` WHERE `name`=? LIMIT 1";


    @Override
    public void create(Companies entity){

        if(entity == null)
            return;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try {
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(CREATE_SQL);
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getRegistrationNumber());
                statement.setString(3, entity.getPhone());
                statement.setString(4, entity.getAddress());
                statement.execute();
                resultSet = statement.getGeneratedKeys();
                if(resultSet.next()){
                    entity.setId(resultSet.getInt(1));
                    addCompaniesProjects(entity.getId(), entity.getProjects());
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public Companies read(Integer key){

        Companies company = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(READ_SQL);
                statement.setInt(1, key);
                resultSet = statement.executeQuery();
                if(resultSet.next()){
                    company = companyFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return company;
    }



    @Override
    public void update(Companies entity){
        if(entity == null){
            return;
        }
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(UPDATE_SQL);
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getRegistrationNumber());
                statement.setString(3, entity.getPhone());
                statement.setString(4, entity.getAddress());
                statement.setLong(5, entity.getId());
                if(statement.execute()){
                    updateCompanyProjects(entity.getId(), entity.getProjects());
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }




    @Override
    public void delete(Integer key){

        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                removeCompaniesProjects(key);
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, key);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public void delete(Companies entity){
        if(entity == null)
            return;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                removeCompaniesProjects(entity.getId());
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, entity.getId());
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }


    @Override
    public List<Companies> getAll(){

        ResultSet resultSet = null;
        Statement statement = null;
        List<Companies> companiesList = new ArrayList<>();
        try {
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.createStatement();
                resultSet = statement.executeQuery(GET_ALL_SQL);
                while (resultSet.next()){
                    companiesList.add(companyFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return companiesList;
    }



    @Override
    public List<Companies> getAllByName(String name){

        ResultSet resultSet =  null;
        PreparedStatement statement = null;
        List<Companies> companyList = new ArrayList<>();
        try {
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(GET_ALL_BY_NAME_SQL);
                statement.setString(1, "%" + name + "%");
                resultSet = statement.executeQuery();
                while(resultSet.next()){
                    companyList.add(companyFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return  companyList;
    }



    @Override
    public Companies getOneByName(String name){

        Companies company = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(GET_ONE_BY_NAME_SQL);
                statement.setString(1, name);
                resultSet = statement.executeQuery();
                if (resultSet.next()){
                    company = companyFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return company;
    }


    /**
     *
     * @param resultSet -
     * @return -
     */
    private Companies companyFromResSet(ResultSet resultSet){
        Companies company = null;
        try{
            int id = resultSet.getInt("id_companies");
            String name = resultSet.getString("name");
            String registrationNumber = resultSet.getString("registration_number");
            String phone = resultSet.getString("phone");
            String address = resultSet.getString("address");
            company = new Companies(id, name, registrationNumber, phone, address, getCompaniesProjects(id));
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }
        return  company;
    }


    /**
     *
     * @param companyId -
     * @return -
     */
    private List<Projects> getCompaniesProjects(int companyId){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Projects> devSkills= new ArrayList<>();

        try{
            if(connection != null){
                StringBuilder sql = new StringBuilder()
                        .append("SELECT proj.`name`, proj.`deadline`, proj.`cost` ")
                        .append("FROM `companies_projects` AS com_proj INNER JOIN `projects` AS proj ON com_proj.`projects_id_projects` = proj.`id_projects`")
                        .append(" WHERE com_proj.`companies_id_companies`=?");
                statement = connection.prepareStatement(sql.toString());
                statement.setLong(1, companyId);
                resultSet = statement.executeQuery();
                while(resultSet.next()){
                    String name = resultSet.getString("name");
                    int deadline = resultSet.getInt("deadline");
                    float cost = resultSet.getFloat("cost");
                    devSkills.add(new Projects(name, deadline, cost));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
        }
        return devSkills;
    }


    /**
     *
     * @param companyId -
     */
    private void addCompaniesProjects(int companyId, List<Projects> companyProjects){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "INSERT INTO `companies_projects` (`companies_id_companies`, `projects_id_projects`) VALUES (?,?)";
                statement = connection.prepareStatement(sql);
                int skill_id;
                for(Projects project: companyProjects){
                    if((skill_id = project.getId()) != 0){
                        statement.setLong(1, companyId);
                        statement.setLong(2, skill_id);
                        statement.executeUpdate();
                    }
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }

    /**
     *
     * @param companyId -
     */
    private void removeCompaniesProjects(int companyId){

        PreparedStatement statement = null;

        try{
            if(connection != null){
                String sql = "DELETE  FROM `companies_projects` WHERE `companies_id_companies`=?";
                statement = connection.prepareStatement(sql);
                statement.setLong(1, companyId);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }

    }


    /**
     *
     * @param companyId -
     */
    private void updateCompanyProjects(int companyId, List<Projects> companyProjects){

        removeCompaniesProjects(companyId);
        addCompaniesProjects(companyId, companyProjects);
    }
}
