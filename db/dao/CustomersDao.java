package db.dao;

import db.DBConnection;
import db.entities.Customers;
import db.entities.Projects;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 */
public class CustomersDao implements BaseDao<Integer, Customers> {

    private Connection connection;

    private static final String CREATE_SQL          = "INSERT INTO `customers` (`name`, `surname`,`email`,`address`,`phone`) VALUES (?,?,?,?,?)";
    private static final String READ_SQL            = "SELECT * FROM `customers` WHERE `id_customers`=?";
    private static final String UPDATE_SQL          = "UPDATE `customers` SET `name`=?, `surname`=?,`email`=?,`address`=?, `phone`=? WHERE  `id_customers`  = ?";
    private static final String DELETE_SQL          = "DELETE FROM `customers` WHERE  `id_customers`  = ?";
    private static final String GET_ALL_SQL         = "SELECT * FROM customers";
    private static final String GET_ALL_BY_NAME_SQL = "SELECT * FROM `customers` WHERE `name` LIKE ?";
    private static final String GET_ONE_BY_NAME_SQL = "SELECT * FROM `customers` WHERE `name`=? LIMIT 1";


    @Override
    public void create(Customers entity){

        if(entity == null)
            return;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(CREATE_SQL);
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getSurname());
                statement.setString(3, entity.getEmail());
                statement.setString(4, entity.getAddress());
                statement.setString(5, entity.getPhone());
                statement.execute();
                resultSet = statement.getGeneratedKeys();
                if(resultSet.next()){
                    entity.setId(resultSet.getInt(1));
                    addCustomerProjects(entity.getId(), entity.getProjects());
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }




    @Override
    public Customers read(Integer key){

        Customers customer = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;

        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(READ_SQL);
                statement.setInt(1, key);
                resultSet = statement.executeQuery();
                if(resultSet.next()){
                    customer = customerFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return customer;
    }



    @Override
    public void update(Customers entity){
        if(entity == null)
            return;

        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(UPDATE_SQL);
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getSurname());
                statement.setString(3, entity.getEmail());
                statement.setString(4, entity.getAddress());
                statement.setString(5, entity.getPhone());
                statement.setLong(6, entity.getId());
                if(statement.execute()){
                    updateCustomerProjects(entity.getId(), entity.getProjects());
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }




    @Override
    public void delete(Integer key){

        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                removeCustomerProjects(key);
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, key);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }





    @Override
    public void delete(Customers entity){

        if(entity == null)
            return;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                removeCustomerProjects(entity.getId());
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, entity.getId());
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public List<Customers> getAll(){

        ResultSet resultSet = null;
        Statement statement = null;
        List<Customers> custList = new ArrayList<>();
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.createStatement();
                resultSet = statement.executeQuery(GET_ALL_SQL);
                while (resultSet.next()){
                    custList.add(customerFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return custList;
    }


    @Override
    public List<Customers> getAllByName(String name){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Customers> customersList = new ArrayList<>();
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(GET_ALL_BY_NAME_SQL);
                statement.setString(1, "%" + name + "%");
                resultSet = statement.executeQuery();

                while(resultSet.next()){
                    customersList.add(customerFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return  customersList;
    }



    /**
     *
     * @param name -
     * @return -
     */
    @Override
    public Customers getOneByName(String name){

        Customers customer = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;

        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(GET_ONE_BY_NAME_SQL);
                statement.setString(1, name);
                resultSet = statement.executeQuery();
                if (resultSet.next()){
                    customer = customerFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return customer;
    }


    /**
     *
     * @param resultSet -
     * @return -
     */
    private Customers customerFromResSet(ResultSet resultSet){

        Customers customer = null;
        try{
            int id = resultSet.getInt("id_customers");
            String name = resultSet.getString("name");
            String surname = resultSet.getString("surname");
            String email = resultSet.getString("email");
            String address = resultSet.getString("address");
            String phone = resultSet.getString("phone");
            customer = new Customers(id, name, surname, email, address, phone, getCustomersProjects(id));
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }
        return customer;
    }


    /**
     *
     * @param customerId - id заказчика
     * @return - возвращает список проектов заказчика
     */
    private List<Projects> getCustomersProjects(int customerId){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Projects> customerProjects= new ArrayList<>();
        try {
            if(connection != null){
                StringBuilder sql = new StringBuilder()
                        .append("SELECT proj.`name`, proj.`deadline`, proj.`cost` ")
                        .append("FROM `customers_projects` AS cust_proj INNER JOIN `projects` AS proj ON cust_proj.`projects_id_projects` = proj.`id_projects`")
                        .append(" WHERE cust_proj.`customers_id_customers`=?");
                statement = connection.prepareStatement(sql.toString());
                statement.setLong(1, customerId);
                resultSet = statement.executeQuery();
                while(resultSet.next()){
                    customerProjects.add(new Projects(
                            resultSet.getString("name"),
                            resultSet.getInt("deadline"),
                            resultSet.getFloat("cost"))
                    );
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
        }
        return customerProjects;
    }


    /**
     *
     * @param customerId - id закзчика
     */
    private void addCustomerProjects(int customerId, List<Projects> customerProjects){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "INSERT INTO `customers_projects` (`customers_id_customers`, `projects_id_projects`) VALUES (?,?)";
                statement = connection.prepareStatement(sql);
                for(Projects project: customerProjects){
                    if(project.getId() != 0){
                        statement.setInt(1, customerId);
                        statement.setInt(2, project.getId());
                        statement.executeUpdate();
                    }
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }



    /**
     *
     * @param customerId - id закзчика
     */
    private void removeCustomerProjects(int customerId){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "DELETE  FROM `customers_projects` WHERE `customers_id_customers`=?";
                statement = connection.prepareStatement(sql);
                statement.setLong(1, customerId);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }


    /**
     *
     * @param customerId - id заказчика
     * @param customerProjects - список проектов клиента
     */
    private void updateCustomerProjects(int customerId, List<Projects> customerProjects){

        removeCustomerProjects(customerId);
        addCustomerProjects(customerId, customerProjects);
    }
}
