package db.dao;

import db.DBConnection;
import db.entities.Developers;
import db.entities.Skills;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class SkillsDao implements BaseDao<Integer, Skills> {

    private Connection connection;

    private static final String CREATE_SQL          = "INSERT INTO `skills` (`name`) VALUES (?)";
    private static final String READ_SQL            = "SELECT * FROM `skills` WHERE `id_skills`=?";
    private static final String UPDATE_SQL          = "UPDATE `skills` SET `name`=? WHERE  `id_skills`  = ?";
    private static final String DELETE_SQL          = "DELETE FROM `skills` WHERE  `id_skills`  = ?";
    private static final String GET_ALL_SQL         = "SELECT * FROM skills";
    private static final String GET_ALL_BY_NAME_SQL = "SELECT * FROM `skills` WHERE `name` LIKE ?";
    private static final String GET_ONE_BY_NAME_SQL = "SELECT * FROM `skills` WHERE `name`=? LIMIT 1";




    /**
     * запись в базу
     * @param entity -
     */
    @Override
    public void create(Skills entity) {

        if(entity == null)
            return;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null) {
                statement = connection.prepareStatement(CREATE_SQL);
                statement.setString(1, entity.getName());
                statement.execute();
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    entity.setId(resultSet.getInt(1));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally{
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }


    /**
     * получаем с базы по id
     * @param key-
     * @return -
     */
    @Override
    public Skills read(Integer key){

        Skills skill = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(READ_SQL);
                statement.setInt(1, key);
                resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    skill = skillFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return skill;
    }

    @Override
    public void update(Skills entity){

        if(entity == null)
            return;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null) {
                statement = connection.prepareStatement(UPDATE_SQL);
                statement.setString(1, entity.getName());
                statement.setLong(2, entity.getId());
                statement.execute();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public void delete(Integer key){

        PreparedStatement statement = null;
        try {
            if((connection = DBConnection.getConnection()) != null) {
                removeDeveloperSkills(key);
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, key);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public void delete(Skills entity){

        if(entity == null)
            return;

        PreparedStatement statement = null;
        try {
            if((connection = DBConnection.getConnection()) != null) {
                removeDeveloperSkills(entity.getId());
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, entity.getId());
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }


    /**
     * получаем список всех скилов
     * @return -
     */
    @Override
    public List<Skills> getAll(){

        ResultSet resultSet = null;
        Statement statement = null;
        List<Skills> skillsList = new ArrayList<>();
        try{
            if((connection = DBConnection.getConnection()) != null) {
                statement = connection.createStatement();
                resultSet = statement.executeQuery(GET_ALL_SQL);
                while (resultSet.next()) {
                    skillsList.add(skillFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally{
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return skillsList;
    }



    @Override
    public List<Skills> getAllByName(String name){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Skills> skillsList = new ArrayList<>();
        try{
            if((connection = DBConnection.getConnection()) != null) {
                statement = connection.prepareStatement(GET_ALL_BY_NAME_SQL);
                statement.setString(1, "%" + name + "%");
                resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    skillsList.add(skillFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return skillsList;
    }



    @Override
    public Skills getOneByName(String name){

        Skills skill = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try {
            if((connection = DBConnection.getConnection()) != null) {
                statement = connection.prepareStatement(GET_ONE_BY_NAME_SQL);
                statement.setString(1, name);
                resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    skill = skillFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return skill;
    }




    /**
     *
     * @param resultSet -
     * @return -
     */
    private Skills skillFromResSet(ResultSet resultSet){
        try{
            int id = resultSet.getInt("id_skills");
            String name = resultSet.getString("name");
            return new Skills(id, name, getDeveloperSkills(id));
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }
        return null;
    }


    /**
     * ф-ция выбирает навыки програмиста из промежуточной таблицы `developer_skills`
     * @param skillId -
     * @return -
     */
    private List<Developers> getDeveloperSkills(int skillId){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Developers> devSkills= new ArrayList<>();

        try{
            if((connection) != null){
                StringBuilder sql = new StringBuilder()
                        .append("SELECT dev.`name`, dev.`skype`, dev.`phone`, dev.`salary` ")
                        .append("FROM `developers_skills` AS dev_sk INNER JOIN `developers` AS dev ON dev_sk.`developers_id_developers` = dev.`id_developers` ")
                        .append("WHERE dev_sk.`skills_id_skills`=?");
                statement = connection.prepareStatement(sql.toString());
                statement.setInt(1, skillId);
                resultSet = statement.executeQuery();
                while(resultSet.next()){
                    String name = resultSet.getString("name");
                    String skype = resultSet.getString("skype");
                    String phone = resultSet.getString("phone");
                    float salary = resultSet.getFloat("salary");
                    devSkills.add(new Developers(name, skype, phone, salary));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
        }
        return devSkills;
    }



    /**
     * ф-ция удаляет навыки програмиста из промежуточной таблицы `developer_skills`
     * @param skillId -
     */
    private void removeDeveloperSkills(int skillId){

        PreparedStatement statement = null;
        try {
            if((connection) != null){
                String sql = "DELETE  FROM `developers_skills` WHERE `skills_id_skills`=?";
                statement = connection.prepareStatement(sql);
                statement.setLong(1, skillId);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }


    }





}
