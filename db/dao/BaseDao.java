package db.dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @param <K>
 * @param <E>
 */
public interface BaseDao <K extends Serializable, E> {

    void create(E entity) throws SQLException;
    E read(K key) throws SQLException;
    List<E> getAllByName(String name) throws SQLException;
    E getOneByName(String name) throws SQLException;
    void update(E entity) throws SQLException;
    void delete(K key) throws SQLException;
    void delete(E entity) throws SQLException;
    List<E> getAll() throws SQLException;

}
