package db.dao;

import db.DBConnection;
import db.entities.Developers;
import db.entities.Projects;
import db.entities.Skills;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by dimas on 28.06.17.
 */
public class DevelopersDao implements BaseDao<Integer, Developers> {

    private Connection connection;

    private static final String CREATE_SQL          = "INSERT INTO `developers` (`name`, `skype`, `phone`,`salary`) VALUES (?,?,?,?)";
    private static final String READ_SQL            = "SELECT * FROM developers WHERE `id_developers`=?";
    private static final String UPDATE_SQL          = "UPDATE `developers` SET `name`=?, `skype`=?, `phone`=?,`salary`=? WHERE  `id_developers`  = ?";
    private static final String DELETE_SQL          = "DELETE FROM developers WHERE  `id_developers`  = ?";
    private static final String GET_ALL_SQL         = "SELECT * FROM `developers`";
    private static final String GET_ALL_BY_NAME_SQL = "SELECT * FROM developers WHERE `name` LIKE ?";
    private static final String GET_ONE_BY_NAME_SQL = "SELECT * FROM `developers` WHERE `name`=? LIMIT 1";


    @Override
    public void create(Developers entity){

        if(entity == null)
            return;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try {
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(CREATE_SQL);
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getSkype());
                statement.setString(3, entity.getPhone());
                statement.setFloat(4, entity.getSalary());
                statement.execute();
                resultSet = statement.getGeneratedKeys();
                if(resultSet.next()){
                    entity.setId(resultSet.getInt(1));
                    addDeveloperSkills(entity.getId(), entity.getSkills());
                    addDeveloperProjects(entity.getId(), entity.getProjects());
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public Developers read(Integer key){

        Developers dev = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try {
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(READ_SQL);
                statement.setInt(1, key);
                resultSet = statement.executeQuery();
                if(resultSet.next()){
                    dev = developerFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return  dev;
    }




    /**
     * обновляет данные о разработчике
     * @param entity -
     */
    @Override
    public void update(Developers entity){
        if(entity == null)
            return;
        PreparedStatement statement = null;
        try {
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(UPDATE_SQL);
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getSkype());
                statement.setString(3, entity.getPhone());
                statement.setFloat(4, entity.getSalary());
                statement.setLong(5, entity.getId());
                if(statement.execute()){
                    updateDeveloperSkills(entity.getId(), entity.getSkills());
                    updateDeveloperProjects(entity.getId(), entity.getProjects());
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }

    /**
     * удаляет данные о разработчике по его id
     * @param key -
     */
    @Override
    public void delete(Integer key){

        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                removeDeveloperSkills(key);
                removeDeveloperProjects(key);
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, key);
                statement.execute();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public void delete(Developers entity){
        if(entity == null)
            return;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                removeDeveloperSkills(entity.getId());
                removeDeveloperProjects(entity.getId());
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, entity.getId());
                statement.execute();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }

    @Override
    public List<Developers> getAll(){

        ResultSet resultSet = null;
        Statement statement = null;
        List<Developers> developersList = new ArrayList<>();
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.createStatement();
                resultSet = statement.executeQuery(GET_ALL_SQL);
                while (resultSet.next()){
                    developersList.add(developerFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }

        return developersList;
    }



    @Override
    public List<Developers> getAllByName(String name){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Developers> devList = new ArrayList<>();
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(GET_ALL_BY_NAME_SQL);
                statement.setString(1, "%" + name + "%");
                resultSet = statement.executeQuery();
                while(resultSet.next()){
                    devList.add(developerFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return  devList;
    }



    @Override
    public Developers getOneByName(String name){

        ResultSet resultSet = null;
        Developers developer = null;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(GET_ONE_BY_NAME_SQL);
                statement.setString(1, name);
                resultSet = statement.executeQuery();
                if (resultSet.next()){
                    developer = developerFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return developer;
    }



    /**
     *
     * @param resultSet -
     * @return -
     */
    private Developers developerFromResSet(ResultSet resultSet){
        Developers developer = null;
        try{
            int id = resultSet.getInt("id_developers");
            String name = resultSet.getString("name");
            String skype = resultSet.getString("skype");
            String phone = resultSet.getString("phone");
            float salary = resultSet.getFloat("salary");
            developer = new Developers(id, name, skype, phone, salary, getDeveloperSkills(id), getDeveloperProjects(id));
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }
        return developer;
    }


    /**
     * ф-ция выбирает навыки програмиста из промежуточной таблицы `developer_skills`
     * @param developerId -
     * @return -
     */
    private List<Skills> getDeveloperSkills(long developerId){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Skills> devSkills= new ArrayList<>();
        try{
            if(connection != null){
                StringBuilder sql = new StringBuilder()
                        .append("SELECT sk.`id_skills`, sk.`name` ")
                        .append("FROM `developers_skills` AS dev_sk INNER JOIN `skills` AS sk ON dev_sk.`skills_id_skills` = sk.`id_skills`")
                        .append(" WHERE dev_sk.`developers_id_developers`=?");
                statement = connection.prepareStatement(sql.toString());
                statement.setLong(1, developerId);
                resultSet = statement.executeQuery();
                while(resultSet.next()){
                    devSkills.add(new Skills(
                                    resultSet.getInt("id_skills"),
                                    resultSet.getString("name")
                            )
                    );
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
        }
        return devSkills;
    }


    /**
     * ф-ция добавляет навыки програмиста в промежуточную таблицу `developer_skills`
     * @param developerId -
     */
    private void addDeveloperSkills(long developerId, List<Skills> devSkills){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "INSERT INTO `developers_skills` (`developers_id_developers`, `skills_id_skills`) VALUES (?,?)";
                statement = connection.prepareStatement(sql);
                int skill_id;
                for(Skills skill: devSkills){
                    skill_id = skill.getId();
                    if(skill_id != 0){
                        statement.setLong(1, developerId);
                        statement.setLong(2, skill.getId());
                        statement.executeUpdate();
                    }
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }

    }

    /**
     * ф-ция добавляет навыки програмиста из промежуточной таблицы `developer_skills`
     * @param developerId -
     */
    private void removeDeveloperSkills(int developerId){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "DELETE  FROM `developers_skills` WHERE `developers_id_developers`=?";
                statement = connection.prepareStatement(sql);
                statement.setLong(1, developerId);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }


    /**
     *
     * @param developerId -
     * @param devSkills -
     */
    private void updateDeveloperSkills(int developerId, List<Skills> devSkills){

        removeDeveloperSkills(developerId);
        addDeveloperSkills(developerId, devSkills);
    }






    /**
     * ф-ция выбирает навыки програмиста из промежуточной таблицы `developer_skills`
     * @param developerId - id заказчика
     * @return - возвращает список проектов заказчика
     */
    private List<Projects> getDeveloperProjects(int developerId){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Projects> developerProjects= new ArrayList<>();
        try{
            if(connection != null){
                StringBuilder sql = new StringBuilder()
                        .append("SELECT proj.`id_projects`, proj.`name`, proj.`deadline`, proj.`cost` ")
                        .append("FROM `developers_projects` AS dev_proj INNER JOIN `projects` AS proj ON dev_proj.`projects_id_projects` = proj.`id_projects`")
                        .append(" WHERE dev_proj.`developers_id_developers`=?");
                statement = connection.prepareStatement(sql.toString());
                statement.setLong(1, developerId);
                resultSet = statement.executeQuery();
                while(resultSet.next()){
                    developerProjects.add(new Projects(
                            resultSet.getInt("id_projects"),
                            resultSet.getString("name"),
                            resultSet.getInt("deadline"),
                            resultSet.getFloat("cost"))
                    );
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
        }
        return developerProjects;
    }


    /**
     * ф-ция добавляет навыки програмиста в промежуточную таблицу `developer_skills`
     * @param developerId - id закзчика
     */
    private void addDeveloperProjects(int developerId, List<Projects> developerProjects){

        PreparedStatement statement = null;
        try {
            if(connection != null){
                String sql = "INSERT INTO `developers_projects` (`developers_id_developers`, `projects_id_projects`) VALUES (?,?)";
                statement = connection.prepareStatement(sql);
                int dev_project_id;
                for(Projects project: developerProjects){
                    if((dev_project_id = project.getId()) != 0){
                        statement.setInt(1, developerId);
                        statement.setInt(2, dev_project_id);
                        statement.executeUpdate();
                    }
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }

    /**
     * ф-ция добавляет навыки програмиста из промежуточной таблицы `developer_skills`
     * @param developerId - id закзчика
     */
    private void removeDeveloperProjects(int developerId){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "DELETE  FROM `developers_projects` WHERE `developers_id_developers`=?";
                statement = connection.prepareStatement(sql);
                statement.setLong(1, developerId);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }


    /**
     * ф-ция добавляет навыки програмиста из промежуточной таблицы `developer_skills`
     * @param developerId - id разработчика
     * @param developerProjects - список проектов клиента
     */
    private void updateDeveloperProjects(int developerId, List<Projects> developerProjects){

        removeDeveloperProjects(developerId);
        addDeveloperProjects(developerId, developerProjects);
    }


}
