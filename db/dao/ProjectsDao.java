package db.dao;

import db.DBConnection;
import db.entities.Companies;
import db.entities.Customers;
import db.entities.Developers;
import db.entities.Projects;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 */
public class ProjectsDao implements BaseDao<Integer, Projects> {

    private Connection connection;

    private static final String CREATE_SQL          = "INSERT INTO `projects` (`name`, `deadline`, `cost`) VALUES (?,?,?)";
    private static final String READ_SQL            = "SELECT * FROM `projects` WHERE `id_projects`=?";
    private static final String UPDATE_SQL          = "UPDATE `projects` SET `name`=?, `deadline`=?, `cost`=? WHERE  `id_projects`  = ?";
    private static final String DELETE_SQL          = "DELETE FROM `projects` WHERE  `id_projects`  = ?";
    private static final String GET_ALL_SQL         = "SELECT * FROM `projects`";
    private static final String GET_ALL_BY_NAME_SQL = "SELECT * FROM `projects` WHERE `name` LIKE ?";
    private static final String GET_ONE_BY_NAME_SQL = "SELECT * FROM `projects` WHERE `name`=? LIMIT 1";


    @Override
    public void create(Projects entity){

        if(entity == null)
            return;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(CREATE_SQL);
                statement.setString(1, entity.getName());
                statement.setInt(2, entity.getDeadline());
                statement.setFloat(3, entity.getCost());
                statement.execute();
                resultSet = statement.getGeneratedKeys();
                if(resultSet.next()){
                    int projectId = resultSet.getInt(1);
                    entity.setId(projectId);
                    addProjectCustomers(projectId, entity.getCustomers());
                    addProjectDevelopers(projectId, entity.getDevelopers());
                    addProjectCompanies(projectId, entity.getCompanies());
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public Projects read(Integer key){

        Projects project = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(READ_SQL);
                statement.setInt(1, key);
                resultSet = statement.executeQuery();
                if(resultSet.next()){
                    project = projectFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return project;
    }

    @Override
    public void update(Projects entity){

        if(entity == null)
            return;
        PreparedStatement statement = null;
        try {
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(UPDATE_SQL);
                statement.setString(1, entity.getName());
                statement.setInt(2, entity.getDeadline());
                statement.setFloat(3, entity.getCost());
                statement.setLong(4, entity.getId());
                if(statement.execute()){
                    int projectId = entity.getId();
                    updateProjectCompanies(projectId, entity.getCompanies());
                    updateProjectCustomers(projectId, entity.getCustomers());
                    updateProjectDevelopers(projectId, entity.getDevelopers());
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public void delete(Integer key){

        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                removeProjectCustomers(key);
                removeProjectDevelopers(key);
                removeProjectCompanies(key);
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, key);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public void delete(Projects entity){
        if(entity == null)
            return;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                int projectId = entity.getId();
                removeProjectCustomers(projectId);
                removeProjectDevelopers(projectId);
                removeProjectCompanies(projectId);
                statement = connection.prepareStatement(DELETE_SQL);
                statement.setLong(1, projectId);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
    }



    @Override
    public List<Projects> getAll(){

        ResultSet resultSet = null;
        Statement statement = null;
        List<Projects> projList = new ArrayList<>();
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.createStatement();
                resultSet = statement.executeQuery(GET_ALL_SQL);
                while (resultSet.next()){
                    projList.add(projectFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return projList;
    }




    @Override
    public List<Projects> getAllByName(String name){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Projects> projectsList = new ArrayList<>();
        try {
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(GET_ALL_BY_NAME_SQL);
                statement.setString(1, "%" + name + "%");
                resultSet = statement.executeQuery();
                while(resultSet.next()){
                    projectsList.add(projectFromResSet(resultSet));
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return  projectsList;
    }



    @Override
    public Projects getOneByName(String name){

        Projects project = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        try{
            if((connection = DBConnection.getConnection()) != null){
                statement = connection.prepareStatement(GET_ONE_BY_NAME_SQL);
                statement.setString(1, name);
                resultSet = statement.executeQuery();
                if (resultSet.next()){
                    project = projectFromResSet(resultSet);
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
            DBConnection.closeConnection(connection);
        }
        return project;
    }


    /**
     *
     * @param resultSet -
     * @return -
     */
    private Projects projectFromResSet(ResultSet resultSet){
        try{
            int id = resultSet.getInt("id_projects");
            String name = resultSet.getString("name");
            int deadline = resultSet.getInt("deadline");
            float cost = resultSet.getFloat("cost");
            return new Projects(id, name, deadline, cost, getProjectDevelopers(id), getProjectCompanies(id), getProjectCustomers(id));
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }
        return null;
    }


    /**
     *
     * @param projectId -
     */
    private List<Companies> getProjectCompanies(int projectId){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Companies> projectCompanies= new ArrayList<>();
        try{
            if(connection != null){
                StringBuilder sql = new StringBuilder()
                        .append("SELECT comp.`id_companies`, comp.`name`, comp.`registration_number`, comp.`phone`, comp.`address` ")
                        .append("FROM `companies_projects` AS comp_proj INNER JOIN `companies` AS comp ON comp_proj.`companies_id_companies` = comp.`id_companies`")
                        .append(" WHERE comp_proj.`projects_id_projects`=?");
                statement = connection.prepareStatement(sql.toString());
                statement.setLong(1, projectId);
                resultSet = statement.executeQuery();
                while(resultSet.next()){
                    projectCompanies.add(new Companies(
                                    resultSet.getInt("id_companies"),
                                    resultSet.getString("name"),
                                    resultSet.getString("registration_number"),
                                    resultSet.getString("phone"),
                                    resultSet.getString("address")
                            )
                    );
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
        }
        return projectCompanies;
    }


    /**
     * ф-ция добавляет компании связанные с проектом в промежуточную таблицу `companies_projects`
     * @param projectId -
     * @param projectCompanies -
     */
    private void addProjectCompanies(int projectId, List<Companies> projectCompanies){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "INSERT INTO `companies_projects` (`companies_id_companies`, `projects_id_projects`) VALUES (?,?)";
                statement = connection.prepareStatement(sql);
                for(Companies company: projectCompanies){
                    if(company.getId() != 0){
                        statement.setLong(1, company.getId());
                        statement.setLong(2, projectId);
                        statement.executeUpdate();
                    }
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }

    /**
     * ф-ция удаляет компании связанные с проектом с промежуточной таблицы `companies_projects`
     * @param projectId - id проекта
     */
    private void removeProjectCompanies(int projectId){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "DELETE  FROM `companies_projects` WHERE `projects_id_projects`=?";
                statement = connection.prepareStatement(sql);
                statement.setLong(1, projectId);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }


    /**
     * ф-ция обновляет компании связанные с проектом в промежуточной таблице `companies_projects`
     * @param projectId -
     * @param projectCompanies -
     */
    private void updateProjectCompanies(int projectId, List<Companies> projectCompanies){

        removeProjectCompanies(projectId);
        addProjectCompanies(projectId, projectCompanies);
    }

    /**
     *
     * @param projectId - id проекта
     * @return -
     */
    private List<Developers> getProjectDevelopers(int projectId){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Developers> projectDevelopers= new ArrayList<>();
        try{
            if(connection != null){
                StringBuilder sql = new StringBuilder()
                        .append("SELECT dev.`name`, dev.`skype`, dev.`phone`, dev.`salary` ")
                        .append("FROM `developers_projects` AS dev_proj INNER JOIN `developers` AS dev ON dev_proj.`projects_id_projects` = dev.`id_developers`")
                        .append(" WHERE dev_proj.`developers_id_developers`=?");
                statement = connection.prepareStatement(sql.toString());
                statement.setLong(1, projectId);
                resultSet = statement.executeQuery();

                while(resultSet.next()){
                    projectDevelopers.add(new Developers(
                            resultSet.getString("name"),
                            resultSet.getString("skype"),
                            resultSet.getString("phone"),
                            resultSet.getFloat("salary"))
                    );
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
        }
        return projectDevelopers;
    }


    /**
     *
     * @param projectId - id проекта
     */
    private void addProjectDevelopers(int projectId, List<Developers> developerProjects){

        PreparedStatement statement = null;

        try {
            if(connection != null){
                String sql = "INSERT INTO `developers_projects` (`developers_id_developers`, `projects_id_projects`) VALUES (?,?)";
                statement = connection.prepareStatement(sql);
                int devId;
                for(Developers project: developerProjects){
                    if((devId = project.getId()) != 0){
                        statement.setInt(1, devId);
                        statement.setInt(2, projectId);
                        statement.executeUpdate();
                    }
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }

    /**
     *
     * @param projectId - id закзчика
     */
    private void removeProjectDevelopers(int projectId){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "DELETE  FROM `developers_projects` WHERE `projects_id_projects`=?";
                statement = connection.prepareStatement(sql);
                statement.setInt(1, projectId);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }


    /**
     *
     * @param projectId - id проекта
     * @param developerProjects - список проектов разработчика
     */
    private void updateProjectDevelopers(int projectId, List<Developers> developerProjects){
        removeProjectDevelopers(projectId);
        addProjectDevelopers(projectId, developerProjects);
    }



    /**
     *
     * @param projectId - id проекта
     * @return - возвращает список заказчиков связанных с этим проектом
     */
    private List<Customers> getProjectCustomers(int projectId){

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Customers> customersProjects= new ArrayList<>();
        try{
            if(connection != null){
                StringBuilder sql = new StringBuilder()
                        .append("SELECT cust.`name`, cust.`surname`, cust.`email`, cust.`address`, cust.`phone` ")
                        .append("FROM `customers_projects` AS cust_proj INNER JOIN `customers` AS cust ON cust_proj.`projects_id_projects` = cust.`id_customers`")
                        .append(" WHERE cust_proj.`projects_id_projects`=?");
                statement = connection.prepareStatement(sql.toString());
                statement.setLong(1, projectId);
                resultSet = statement.executeQuery();

                while(resultSet.next()){
                    customersProjects.add(new Customers(
                            resultSet.getString("name"),
                            resultSet.getString("surname"),
                            resultSet.getString("email"),
                            resultSet.getString("address"),
                            resultSet.getString("phone"))
                    );
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeResultSet(resultSet);
            DBConnection.closeStatement(statement);
        }
        return customersProjects;
    }


    /**
     *
     * @param projectId - id проекта
     * @param customersProjects -
     */
    private void addProjectCustomers(int projectId, List<Customers> customersProjects){

        PreparedStatement statement = null;
        try{
            if(connection != null){
                String sql = "INSERT INTO `customers_projects` (`customers_id_customers`, `projects_id_projects`) VALUES (?,?)";
                statement = connection.prepareStatement(sql);
                int cust_id;
                for(Customers project: customersProjects){
                    if((cust_id = project.getId()) != 0){
                        statement.setInt(1, cust_id);
                        statement.setInt(2, projectId);
                        statement.executeUpdate();
                    }
                }
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }

    /**
     *
     * @param projectId - id проекта
     */
    private void removeProjectCustomers(int projectId){

        PreparedStatement statement = null;
        try {
            if(connection != null){
                String sql = "DELETE  FROM `customer_projects` WHERE `projects_id_projects`=?";
                statement = connection.prepareStatement(sql);
                statement.setLong(1, projectId);
                statement.executeUpdate();
            }
        }catch (SQLException e){
            //logger.log(Level.INFO, e.getMessage());
        }finally {
            DBConnection.closeStatement(statement);
        }
    }


    /**
     *
     * @param projectId - id проекта
     * @param customerProjects - список проектов клиента
     */
    private void updateProjectCustomers(int projectId, List<Customers> customerProjects){

        removeProjectCustomers(projectId);
        addProjectCustomers(projectId, customerProjects);
    }


}
